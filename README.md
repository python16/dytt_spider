###创建一个新存储库
* git clone https://gitlab.com/python16/dytt_spider.git
* cd dytt_spider
* touch README.md
* git add README.md
* git commit -m "add README"
* git push -u origin master

###推送现有文件夹
* cd existing_folder
* git init
* git remote add origin https://gitlab.com/python16/dytt_spider.git
* git add .
* git commit -m "Initial commit"
* git push -u origin master

###推送现有的 Git 存储库
* cd existing_repo
* git remote rename origin old-origin
* git remote add origin https://gitlab.com/python16/dytt_spider.git
* git push -u origin --all
* git push -u origin --tags